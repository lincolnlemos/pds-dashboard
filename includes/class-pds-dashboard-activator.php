<?php

/**
 * Fired during plugin activation
 *
 * @link       https://lincolnlemos.com
 * @since      1.0.0
 *
 * @package    Pds_Dashboard
 * @subpackage Pds_Dashboard/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pds_Dashboard
 * @subpackage Pds_Dashboard/includes
 * @author     Lincoln Lemos <hi@lincolnlemos.com>
 */
class Pds_Dashboard_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
