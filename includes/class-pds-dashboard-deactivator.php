<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://lincolnlemos.com
 * @since      1.0.0
 *
 * @package    Pds_Dashboard
 * @subpackage Pds_Dashboard/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pds_Dashboard
 * @subpackage Pds_Dashboard/includes
 * @author     Lincoln Lemos <hi@lincolnlemos.com>
 */
class Pds_Dashboard_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
