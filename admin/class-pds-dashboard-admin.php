<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://lincolnlemos.com
 * @since      1.0.0
 *
 * @package    Pds_Dashboard
 * @subpackage Pds_Dashboard/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Pds_Dashboard
 * @subpackage Pds_Dashboard/admin
 * @author     Lincoln Lemos <hi@lincolnlemos.com>
 */
class Pds_Dashboard_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The array of templates that this plugin tracks.
	 */
	protected $templates;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		// Add the default templates
		$this->templates = array(
			'pds-dashboard-page-template.php' => 'PDS: Dashboard',
		);

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pds_Dashboard_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pds_Dashboard_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/pds-dashboard-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pds_Dashboard_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pds_Dashboard_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/pds-dashboard-admin.js', array( 'jquery' ), $this->version, false );

	}


	/**
	 * Adds our templates to the page dropdown
	 *
	 * @param array $posts_templates
	 * @return array
	 * @since 	1.0.0
	 */
	public function add_new_template($posts_templates) {
		$posts_templates = array_merge( $posts_templates, $this->templates );
		return $posts_templates;
	}


	/**
	 * Checks if the template is assigned to the page
	 */
	public function view_pds_template( $template ) {
		
		// Get global post
		global $post;

		// Return template if post is empty
		if ( ! $post ) {
			return $template;
		}

		// Return default template if we don't have a custom one defined
		if ( !isset( $this->templates[get_post_meta( $post->ID, '_wp_page_template', true )] ) ) {
			return $template;
		} 

		$file = PDS_DASHBOARD_PUBLIC_PATH_TEMPLATES . get_post_meta( $post->ID, '_wp_page_template', true );
		
		// Just to be safe, we check if the file exist first
		if ( file_exists( $file ) ) {
			return $file;
		} else {
			echo $file;
		}

		// Return template
		return $template;

	}

	public function register_user_metas() {
		register_meta( 'user', 'phone', [ 'show_in_rest' => true ] );
		register_meta( 'user', 'jobrole', [ 'show_in_rest' => true ] );
		register_meta( 'user', 'type', [ 'show_in_rest' => true ] );
	}	

	public function jwt_auth_token_before_dispatch_filter($data, $user) {		

		$data['onboarding'] = get_user_meta( $user->data->ID, 'onboarding', true );
		$data['data'] = $user->data;
		$data['meta'] = array_merge($user->meta, $this->get_user_metadata($user->data->ID));
		$data['userid'] = $user->data->ID;

		return $data;
	}

	public function rest_response_at_user_update( $response, $user, $request ) {
		
		$response->data['meta'] = array_merge($response->data['meta'], $this->get_user_metadata($user->ID));
		$response->data['data'] = $user->data;
		$response->data['onboarding'] = get_user_meta( $user->data->ID, 'onboarding', true );

		return $response;
	}

	public function get_user_metadata($user_id) {
		// Get all user meta data for $user_id
		$meta = get_user_meta( $user_id );
		
		// Filter out empty meta data
		$meta = array_filter( array_map( function( $a ) {
				return $a[0];
		}, $meta ) );

		return $meta;
	}	
}
