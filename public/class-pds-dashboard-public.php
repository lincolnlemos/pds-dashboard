<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://lincolnlemos.com
 * @since      1.0.0
 *
 * @package    Pds_Dashboard
 * @subpackage Pds_Dashboard/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Pds_Dashboard
 * @subpackage Pds_Dashboard/public
 * @author     Lincoln Lemos <hi@lincolnlemos.com>
 */
class Pds_Dashboard_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pds_Dashboard_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pds_Dashboard_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/pds-dashboard-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pds_Dashboard_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pds_Dashboard_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/pds-dashboard-public.js', array( 'jquery' ), $this->version, false );
	 
	}

	public function ajax_login() {

		// Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

		$user_signon = wp_signon( $info, false );
		
    if ( is_wp_error($user_signon) ){
			wp_send_json_error([
				'msg' => 'Login inválidos. Tenta novamente.'
			]);
    } else {
			wp_send_json_success([
				'nonce' => wp_nonce_field( 'ajax-login-nonce', 'security' )
			]);
    }

		wp_die();
		
	}

	/**
	 * Add the endpoints to the API
	 */
	public function add_api_routes() {
		/**
		 * Handle Updater User request.
		 */
		register_rest_route('pds-dashboard', '/changepass', array(
			'methods' => 'POST',
			'callback' => array($this, 'wp_rest_update_user_password'),
		));

		
		/**
		 * Get the comments from a user
		 */
		register_rest_route('wp/v2', '/users/comments', array(
			'methods' => 'GET',
			'callback' => array($this, 'wp_rest_get_user_comments'),
		));


	}
	
	public function wp_rest_update_user_password($request = null) {

		$user = wp_get_current_user();
		$response = array();
		$parameters = $request->get_json_params();

		$currentPassword = sanitize_text_field($parameters['currentPassword']);
		$password = sanitize_text_field($parameters['password']);
		
		$error = new WP_Error();
		
		if (empty($user)) {
			$error->add(401, __("Não conseguimos conectar sua conta. Por favor, faça o login novamente.", 'wp-rest-user'), array('status' => 401));
			return $error;
		}		
		if (empty($currentPassword)) {
			$error->add(400, __("Por favor, insira a senha atual.", 'wp-rest-user'), array('status' => 400));
			return $error;
		}				
		if (empty($password)) {
			$error->add(400, __("Por favor, insira a nova senha.", 'wp-rest-user'), array('status' => 400));
			return $error;
		}
		
		if ($user && wp_check_password( $currentPassword, $user->data->user_pass, $user->ID ) ) {
			
			wp_set_password($password, $user->ID);

			return new WP_REST_Response([
				'message' => 'Senha alterada com sucesso!',
				'code' => 200
			], 200);

		} else {
			$error->add(400, __("A senha atual informada não corresponde com o usuário.", 'wp-rest-user'), array('status' => 400));
			return $error;
		}
		
	}
	

	public function wp_rest_get_user_comments($request = null) {
		
		if (is_user_logged_in()) {
			
			$user = wp_get_current_user();

			
			$args = [
				'include_unapproved' => true,
				'user_id' => $user->ID,
				// 'hierarchical' => 'threaded'
			];

			$commentsQuery = get_comments( $args );
			
			if (count($commentsQuery) > 0) {
				$comments = [];
				foreach ($commentsQuery as $c ) {

					if ($comment->comment_parent) {
						$comments[$c->comment_ID]['reply']['content'] = $c->comment_content;
					} else {
						$comments[$c->comment_ID] = [
							'id' => $c->comment_post_ID,
							'title' => get_the_title($c->comment_post_ID),
							'content' => $c->comment_content,
							'url' => get_permalink($c->comment_post_ID),
							'rate' => get_comment_meta( $c->comment_ID, 'rating', true ),
							'date' => get_comment_date('d/m/Y H:i', $c),
							'comment_approved' => $c->comment_approved,
						];
					} // else

				}

				$comments = array_reduce($comments,function($previous, $current){
					$previous[] = $current;
					return $previous;
				}, []);

			} else {
				$comments = false;
			}

			return new WP_REST_Response([
				'comments' => $comments,
				// 'query' => $commentsQuery,
				'code' => 200
			], 200);

		} else {
			return new WP_Error('get_comments_without_user', 'Sua sesão foi expirada, por favor faça o login novamente.', array('status' => 401));
		}

	}
	
}
