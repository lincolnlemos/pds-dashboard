module.exports = {
  productionSourceMap: false,
  publicPath: process.env.NODE_ENV === 'production'
      ? '/dashboard/'
      : '/',
  outputDir: '../dist',
  filenameHashing: false,
  configureWebpack: {
      devServer: {
          contentBase: '/wp-content/plugins/pds-dashboard/public/vue/src/assets',
          allowedHosts: ['pontodesaude.partnerprogrammer.com'],
          headers: {
              'Access-Control-Allow-Origin': '*'
          },
      },
      externals: {
          jquery: 'jQuery'
      },
      output: {
          filename: 'js/[name].js',
          chunkFilename: 'js/[name].js',
      },
  },
  css: {
      extract: {
          filename: 'css/[name].css',
          chunkFilename: 'css/[name].css'
      }
  },
};