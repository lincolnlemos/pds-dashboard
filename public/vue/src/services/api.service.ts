import axios from 'axios';

const baseURL = process.env.VUE_APP_API_URL

function getToken() {  
  const userData : any = localStorage.getItem('user');
  let user = JSON.parse(userData);

  if (user && user.token) {
    return user.token;
  } else {
    return false;
  }
}


const http = axios.create({
  baseURL,
})

http.interceptors.request.use (
  function (config) {
    const token = getToken();
    if (token) config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  function (error) {
    return Promise.reject (error);
  }
);

export default http