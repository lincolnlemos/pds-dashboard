import { ONBOARDING_STATUS, USER_TYPES } from '../models/enums';
import api from './api.service';
import UserService from './user.service';

const API_URL =  process.env.VUE_APP_API_URL;


class AuthService {

  hasToken() {
    const local = localStorage.getItem('user');
    const user = local ? JSON.parse(local) : null;

    return user.token
  }

  login(credentials : any ) {
    return this.loginApiCall(credentials)
      .then(this.handleResponse)
      .then(response => {

        if (response.data.token) {
          localStorage.setItem('user', JSON.stringify({
            token : response.data.token,
            userid : response.data.userid
          }));
        }

        return response.data;
      });
  }

  loginApiCall(credentials: any) {
    return api.post(API_URL + 'jwt-auth/v1/token ', {
        username: credentials.email,
        password: credentials.password
      })
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user: any) {
    return api.post(API_URL + 'wp/v2/users/register', {
      username: user.email,
      email: user.email,
      password: user.password
    }).then( (response) => {
      return this.login(user)
    }).then( (response) => {
      
      let data : any = { meta: {} };
      
      if (user.nickname) {
        data.nickname = user.nickname;
      }
      
      if (user.name) {
        data.name = user.name;
      }
      
      if (user.jobrole) {
        data.meta.jobrole = user.jobrole;
      }
      
      if (user.type) {
        data.meta.type = user.type;

        if (user.type == USER_TYPES.PACIENTE ) {
          data.meta.onboardingStatus = ONBOARDING_STATUS.DONE;
        }

      }

      return UserService.update(data)
    })
  }

  handleResponse(response: any) {
    if (response.status === 401) {
      this.logout();
      location.reload(true);

      const error = response.data && response.data.message;
      return Promise.reject(error);
    }

    return Promise.resolve(response);
  }
}

export default new AuthService();