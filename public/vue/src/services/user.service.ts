import api from './api.service';

class UserService {

  userId() {
    const userData : any = localStorage.getItem('user');
    let user = JSON.parse(userData);
    return user.userid;
  }
  
  get() {    
    return api.get(`wp/v2/users/${this.userId()}`)
  }

  update(data : any) {
    return api.put(`wp/v2/users/${this.userId()}`, data)
  }

  updatePassword(data: any) {
    return api.post(`pds-dashboard/changepass`, { userid: this.userId(), ...data })
  }
  
}

export default new UserService();