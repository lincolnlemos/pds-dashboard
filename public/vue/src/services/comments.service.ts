import api from './api.service';

class CommentsService {

  userId() {
    const userData : any = localStorage.getItem('user');
    let user = JSON.parse(userData);
    return user.userid;
  }
  
  getByUser() {    
    return api.get(`wp/v2/users/comments`)
  }
  
}

export default new CommentsService();