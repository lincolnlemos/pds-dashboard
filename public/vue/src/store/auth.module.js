import AuthService from '../services/auth.service'
import { USER_TYPES } from '../models/enums'
import userService from '../services/user.service'
import store from '@/store'

// const local = localStorage.getItem('user');
// const user = local ? JSON.parse(local) : null;

// const initialState = user
//   ? { status: { loggedIn: true }, user }
//   : { status: {}, user: null };  

export const auth = {
  namespaced: true,
  state: {
    status: {},
    user: null
  },
  actions: {

    login({ commit }, user) {
      return AuthService.login(user).then(
        response => {
          commit('loginSuccess', response);
          return Promise.resolve(response);
        },
        error => {
          commit('loginFailure');
          return Promise.reject(error.response.data);
        }
      );
    },

    logout({ commit }) {
      AuthService.logout();
      commit('logout');
    },

    register({ commit, dispatch }, user) {
      return AuthService.register(user).then(
        response => {
          commit('registerSuccess');
          return dispatch('login', user);
        },
        error => {
          commit('registerFailure');
          return Promise.reject(error.response.data);
        }
      );
    },

    userUpdate({ commit }, user ) {
      return userService.update(user).then(
        response => {
          commit('userUpdatedSuccess', response.data)
          return Promise.resolve(response.data)
        },
        error => {
          commit('userUpdatedFailure')
          return Promise.reject(error.response.data)
        }
      )
    },

    fetchUser({ commit } ) {
      return userService.get().then(
        response => {
          commit('userFechSuccess', response.data)
          return Promise.resolve(response.data)
        },
        error => {
          commit('userFechFailure')
          return Promise.reject(error.response.data)
        }
      )
    }
    
  },

  mutations: {
    loginSuccess(state, user) {
      state.status = { loggedIn: true }
      state.user = user
    },
    loginFailure(state) {
      state.status = {}
      state.user = null
    },
    logout(state) {
      state.status = {}
      state.user = null
    },
    registerSuccess(state, user) {
    },
    registerFailure(state) {
      state.status = {}
    },
    userUpdatedSuccess(state, userData) {
      state.user = userData
    },
    userFechSuccess(state, userData) {
      state.user = userData
    },    
    userFechFailure(state, user) {

    },

  },

  getters: {
    namePlaceholder : (state) => {
      return 'Nome Completo'      
    },
    
    isAuthenticated : (state) => {
      return (state.status || null ).loggedIn || null
    },

    userType : (state) => {
      return state.user.meta.type || null
    }

    // userForm: (state) => {
    //   return {
    //     name: state.user.data.display_name,
    //     email: state.user.data.user_email,
    //     nickname: state.user.meta.nickname,
    //     phone: state.user.meta.phone
    //   }
    // }
  }

};