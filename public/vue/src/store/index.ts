import Vue from 'vue';
import Vuex from 'vuex';

const { auth } = require('./auth.module');
const { onboarding } = require('./onboarding.module');

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    onboarding
  }
})
