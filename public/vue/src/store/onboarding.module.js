
export const onboarding = {
  namespaced: true,
  state: {
    data: {},
    step: ''
  },
  actions: {

    addData({commit}, data) {
      commit('addData', data)
    }

  },

  mutations: {    
    addData(state, data) {
      state.data = {...state.data, ...data}
    },
  },

  getters: {
    
  }

};