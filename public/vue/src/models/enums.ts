export const enum USER_TYPES {
  PACIENTE =  'paciente',
  PROFISSIONAL = 'profissional',
  ESTABELECIMENTO = 'estabelecimento'
}

export const enum ONBOARDING_STATUS {
  TODO =  'todo',
  DONE = 'done'
}