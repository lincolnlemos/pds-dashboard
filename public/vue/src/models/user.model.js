import { required, sameAs, email, minLength } from 'vuelidate/lib/validators'

export default class User {
  constructor(username, email, password) {
    this.username = username;
    this.email = email;
    this.password = password;
  }
}

export const userFormValidation = {
  email: {
    required,
    email
  },
  name: {
    required
  },
  nickname: {
    
  },
  phone: {
    
  },
  jobrole: {
    
  },
  password: {
    required,
    minLength: minLength(8)
  },
  passwordConfirmation: {
    required,
    sameAsPassword: sameAs('password')
  },
  termos: {
    required,
    sameAs: sameAs(() => true)
  }
}