import Vue from 'vue';
import VueRouter from 'vue-router';

import DashboardChangePassword from '@/components/DashboardChangePassword.vue';
import DashboardHome from '@/components/DashboardHome.vue';
import DashboardMyAccount from '@/components/DashboardMyAccount.vue';
import DashboardMyComments from '@/components/DashboardMyComments.vue';
import DashboardOnboarding from '@/components/DashboardOnboarding.vue';
import OnboardingFormDados from '@/components/OnboardingFormDados.vue';
import OnboardingFormEnderecos from '@/components/OnboardingFormEnderecos.vue';
import store from '@/store';
import Dashboard from '@/views/Dashboard.vue';
import LoginPage from '@/views/LoginPage.vue';
import RegisterPage from '@/views/RegisterPage.vue';

// const Store : any = store;

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: 'cadastro/paciente'
  },
  {
    path: '/cadastro/:type',
    name: 'cadastro',
    component: RegisterPage,
  },
  {
    path: '/login',
    name: 'login',
    component: LoginPage,
  },
  {
    path: '/recuperar-senha',
    name: 'recuperar-senha',
    component: LoginPage,
  },
  
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '/',
        name: 'dashboard-index',
        component: DashboardHome
      },
      {
        path: 'meus-dados',
        name: 'dashboard-my-account',
        component: DashboardMyAccount
      },
      {
        path: 'alterar-senha',
        name: 'dashboard-change-pass',
        component: DashboardChangePassword
      },
      {
        path: 'avaliacoes',
        name: 'dashboard-reviews',
        component: DashboardMyComments
      },
    ]
  },

  {
    path: '/onboarding',
    name: 'onboarding',
    component: DashboardOnboarding,
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '/',
        name: 'onboarding-dados',
        component: OnboardingFormDados
      },      
      {
        path: 'endereco',
        name: 'onboarding-enderecos',
        component: OnboardingFormEnderecos
      },

    ]
  },

  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
  {
    path: '**',    
    redirect: '/'
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) { 
    
    const local = localStorage.getItem('user');
    const user = local ? JSON.parse(local) : null;
    const isAuthenticated = store.getters['auth/isAuthenticated'];
    // Se existir o token e estiver autenticado
    if (user && isAuthenticated) {
      next()
    // Se existir o token mas não estiver autenticado
    } else if (user) {
      store.dispatch('auth/fetchUser').then(
        response => {
          next()
        },
        error => {
          // TODO - error handling
          console.log('error', error)
        }
      )
    } else {
      next({ 
        path: '/login', 
        query: { redirect: to.fullPath } 
      }) 
    }

  } else { 
      next() // make sure to always call next()! 
  } 
})

export default router
