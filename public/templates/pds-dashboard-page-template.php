<?php 
  $pds = new Pds_Dashboard();  
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- <link rel="icon" href="<%= BASE_URL %>favicon.ico"> -->
    <title>vue</title>    
  </head>
  <body>
    <noscript>
      <strong>We're sorry but vue doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
    <div id="app"></div>
    <!-- built files will be auto injected -->    
    <?php 
      if ($pds->is_developer_server()) : 
        echo '<script type="text/javascript" src="http://localhost:8080/js/chunk-vendors.js"></script>';
        echo '<script type="text/javascript" src="http://192.168.0.15:8080/js/app.js"></script>';
      else:
        echo '<script type="text/javascript" src="'. PDS_DASHBOARD_PUBLIC_URL . 'dist/js/chunk-vendors.js?version='. $pds->get_version().'"></script>';        
        echo '<script type="text/javascript" src="'. PDS_DASHBOARD_PUBLIC_URL . 'dist/js/app.js?version='. $pds->get_version().'"></script>';
        echo '<script type="text/javascript" src="'. PDS_DASHBOARD_PUBLIC_URL . 'dist/js/about.js?version='. $pds->get_version().'"></script>';
        echo '<link href="'. PDS_DASHBOARD_PUBLIC_URL . 'dist/css/app.css?version='. $pds->get_version().'"" rel="stylesheet"></head>';
      endif;
    ?>			

  </body>
</html>
