<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://lincolnlemos.com
 * @since             1.0.0
 * @package           Pds_Dashboard
 *
 * @wordpress-plugin
 * Plugin Name:       PDS Dashboard
 * Plugin URI:        https://pontodesaude.com.br
 * Description:       Configura e exibe toda área do Dashboard para pacientes, médicos e clínicas.
 * Version:           1.0.0
 * Author:            Lincoln Lemos
 * Author URI:        https://lincolnlemos.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pds-dashboard
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PDS_DASHBOARD_VERSION', '1.0.0' );
define( 'PDS_DASHBOARD_PUBLIC_URL', plugins_url() . '/pds-dashboard/public/' );
define( 'PDS_DASHBOARD_PUBLIC_PATH', plugin_dir_path( __FILE__ ) . 'public/' );
define( 'PDS_DASHBOARD_PUBLIC_PATH_TEMPLATES', plugin_dir_path( __FILE__ ) . 'public/templates/' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-pds-dashboard-activator.php
 */
function activate_pds_dashboard() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pds-dashboard-activator.php';
	Pds_Dashboard_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-pds-dashboard-deactivator.php
 */
function deactivate_pds_dashboard() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pds-dashboard-deactivator.php';
	Pds_Dashboard_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_pds_dashboard' );
register_deactivation_hook( __FILE__, 'deactivate_pds_dashboard' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-pds-dashboard.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_pds_dashboard() {

	$plugin = new Pds_Dashboard();
	$plugin->run();

}
run_pds_dashboard();
